# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171014073030) do

  create_table "journal_entries", force: :cascade do |t|
    t.text     "past_physical_reflection"
    t.text     "past_mental_reflection"
    t.text     "goal_progress"
    t.string   "half_round",                     default: "EEEEEEEEEEEEEEE"
    t.string   "conditions"
    t.text     "coach_advice"
    t.text     "individual_activity_reflection"
    t.text     "lesson_reflection"
    t.text     "what_i_did_well_today"
    t.text     "future_physical_reflection"
    t.text     "future_mental_reflection"
    t.date     "entry_date"
    t.integer  "user_id"
    t.datetime "created_at",                                                 null: false
    t.datetime "updated_at",                                                 null: false
  end

  add_index "journal_entries", ["user_id"], name: "index_journal_entries_on_user_id"

  create_table "users", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.string   "remember_digest"
    t.string   "password_digest"
    t.string   "activation_digest"
    t.boolean  "activated",         default: false
    t.datetime "activated_at"
    t.string   "reset_digest"
    t.datetime "reset_sent_at"
    t.boolean  "admin",             default: false
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true

end
