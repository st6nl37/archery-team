class CreateJournalEntries < ActiveRecord::Migration
  def change
    create_table :journal_entries do |t|
      t.text    :past_physical_reflection
      t.text    :past_mental_reflection
      t.text    :goal_progress
      t.string  :half_round, default: "EEEEEEEEEEEEEEE"
      t.string  :conditions
      t.text    :coach_advice
      t.text    :individual_activity_reflection
      t.text    :lesson_reflection
      t.text    :what_i_did_well_today
      t.text    :future_physical_reflection
      t.text    :future_mental_reflection
      t.date    :entry_date

      t.references :user, index: true, foreign_key: true
      
      t.timestamps null: false
    end
  end
end
