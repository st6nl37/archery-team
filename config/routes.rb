Rails.application.routes.draw do
  root   'static_pages#home'

  get    'signup'         => 'users#new'
  post   'users/promote'  => 'users#change_admin'
  resources :users

  get 'password_resets/new'
  get 'password_resets/edit'
  resources :password_resets, only: [:new, :create, :edit, :update]

  get 'account_activations/edit'
  resources :account_activations, only: [:edit]

  get    'sessions/new'
  get    'login_as/:id' => 'sessions#login_as'
  get    'login'        => 'sessions#new'
  post   'login'        => 'sessions#create'
  delete 'logout'       => 'sessions#destroy'
  
  get    'users/:user_id/journal_entries' => 'journal_entries#index', as: :journal_entries
  post   'users/:user_id/journal_entries' => 'journal_entries#create'
  get    'users/:user_id/journal_entries/:id' => 'journal_entries#show', as: :journal_entry
  get    'users/:user_id/journal_entries/:id/edit' => 'journal_entries#edit', as: :edit_journal_entry
  patch  'users/:user_id/journal_entries/:id' => 'journal_entries#update', as: :update_journal_entry
#  put    'users/:user_id/journal_entries/:id' => 'journal_entries#update', as: :update_journal_entry
  delete 'users/:user_id/journal_entries/:id' => 'journal_entries#destroy', as: :delete_journal_entry
end
