class ApplicationMailer < ActionMailer::Base
  default from: "archery@ucla.edu"
  layout 'mailer'
end
