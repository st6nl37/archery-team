class UsersController < ApplicationController
  before_action :logged_in_user, only: [:index, :show, :edit, :update, :destroy]
  before_action :correct_user,   only: [:edit, :update]
  before_action :admin_user,     only: [:index, :destroy, :change_admin, :activate_user]
  helper_method :sort_column, :sort_direction
  
  def index
    @users = User.order(sort_column + " " + sort_direction).paginate(page: params[:page])
  end
  
  def show
    @user = User.find(params[:id])
  end
  
  def new
    @user = User.new
  end
  
  def create
    @user = User.new(user_params)
    #if verify_recaptcha(model: @user) && @user.save
    if @user.save
      @user.send_activation_email
      add_flash_message( :info, t('.activation_email_notice') )
      redirect_to root_url
    else
      render 'new'
    end
  end
  
  def edit
  end
  
  def update
    if @user.update_attributes(user_params)
      add_flash_message( :success, t('.update_success') )
      redirect_to @user
    else
      render 'edit'
    end
  end
  
  def destroy
    User.find(params[:id]).destroy
    add_flash_message( :success, t('.delete_success') )
    redirect_to users_url
  end
  
  def change_admin
    user = User.find(params[:user_id])
    if !current_user?(user)
      user.update_attributes(admin: !user.admin)
    end
    redirect_to user
  end

  def activate_user
    user = User.find(params[:user_id])
    if !current_user?(user)
      user.update_attributes(admin: !user.admin)
    end
    redirect_to user
  end

  private
    def user_params
      params.require(:user).permit(:first_name, 
                                   :last_name, 
                                   :email, 
                                   :password,
                                   :password_confirmation)
    end
    
    # Confirms the correct user.
    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless current_user?(@user) || current_user.admin?
    end

    def sort_column
      User.column_names.include?(params[:sort]) ? params[:sort] : "created_at"
    end
    
    def sort_direction
      %w[asc desc].include?(params[:direction]) ? params[:direction] : "desc"
    end
end
