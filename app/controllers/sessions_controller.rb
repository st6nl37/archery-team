class SessionsController < ApplicationController
  before_action :logged_in_user, only: [:login_as]
  before_action :admin_user, only: [:login_as]
  
  def new
  end
  
  def create
    user = User.find_by(email: params[:session][:email])
    if user && user.authenticate(params[:session][:password])
      if user.activated?
        log_in user
        params[:session][:remember_me] == '1' ? remember(user) : forget(user)
        redirect_back_or user
      else
        add_flash_message( :warning, t('.account_not_activated') )
        redirect_to root_url
      end
    else
      flash.now[:danger] = [t('.invalid_password')]
      render 'new'
    end
  end
  
  def destroy
    log_out if logged_in?
    redirect_to root_url
  end

  def login_as
    user = User.find(params[:id])
    if user.nil?
      redirect_to root_url
    elsif user.activated?
      log_in user
      forget(user)
      redirect_back_or user
    else
      add_flash_message( :warning, t('.account_not_activated') )
      redirect_to root_url
    end
  end
end
