class PasswordResetsController < ApplicationController
  before_action :get_user,         only: [:edit, :update]
  before_action :valid_user,       only: [:edit, :update]
  before_action :check_expiration, only: [:edit, :update]

  def new
  end

  def create
    @user = User.find_by(email: params[:password_reset][:email].downcase)
    if @user && @user.activated?
      @user.create_reset_digest
      @user.send_password_reset_email
      add_flash_message( :info, t('.password_reset_sent') )
      redirect_to root_url
    elsif @user && !@user.activated?
      flash.now[:danger] = [t('.account_not_activated')]
      @user.send_new_activation_email
      render 'new'
    else
      flash.now[:danger] = [t('.email_not_found')]
      render 'new'
    end
  end

  def edit
  end

  def update
    if params[:user][:password].empty?
      @user.errors.add(:password, t('.empty_error'))
      render 'edit'
    elsif @user.update_attributes(user_params)
      log_in @user
      add_flash_message( :success, t('.password_reset_confirmation') )
      redirect_to @user
    else
      render 'edit'
    end
  end

  private

    def user_params
      params.require(:user).permit(:password, :password_confirmation)
    end

    # Before filters

    def get_user
      @user = User.find_by(email: params[:email])
    end

    # Confirms a valid user.
    def valid_user
      unless (@user && @user.activated? &&
              @user.authenticated?(:reset, params[:id]))
        redirect_to root_url
      end
    end

    # Checks expiration of reset token.
    def check_expiration
      if @user.password_reset_expired?
        add_flash_message( :danger, t('.password_reset_expired') )
        redirect_to new_password_reset_url
      end
    end
end
