class JournalEntriesController < ApplicationController
  before_action :logged_in_user
  before_action :correct_user
  helper_method :sort_column, :sort_direction
  
  def index
    @user = User.find(params[:user_id])
    @journal_entries = @user.journal_entries.order(sort_column + " " + sort_direction).paginate(page: params[:page])
    @journal_entry = JournalEntry.new
  end
  
  def show
    @journal_entry = JournalEntry.find(params[:id])
  end

  def create
    @journal_entry = current_user.journal_entries.build(entry_date: Date.today)
    if @journal_entry.save
      redirect_to edit_journal_entry_url(id: @journal_entry, user_id: current_user)
    else
      redirect_to journal_entries_url(user_id: current_user)
    end
  end
  
  def edit
    @journal_entry = JournalEntry.find(params[:id])
  end
  
  def update
    @journal_entry = JournalEntry.find(params[:id])
    if @journal_entry.update_attributes(journal_entry_params)
      #add_flash_message( :success, t('.update_success') )
      respond_to do |format|
        format.html { redirect_to journal_entries_url(user_id: current_user) }
        format.js
      end
    else
      render 'edit'
    end
  end
  
  def destroy
    JournalEntry.find(params[:id]).destroy
    add_flash_message( :success, "Journal Entry Deleted" )
    redirect_to journal_entries_url(user_id: current_user)
  end
  
  private
    def journal_entry_params
      params.require(:journal_entry).permit(:past_physical_reflection,
                                            :past_mental_reflection,
                                            :goal_progress,
                                            :half_round,
                                            :conditions,
                                            :coach_advice,
                                            :individual_activity_reflection,
                                            :lesson_reflection,
                                            :what_i_did_well_today,
                                            :future_physical_reflection,
                                            :future_mental_reflection,
                                            :entry_date)
    end
    
    # Confirms the correct user.
    def correct_user
      @user = User.find(params[:user_id])
      redirect_to(root_url) unless current_user?(@user) || current_user.admin?
    end

    def sort_column
      JournalEntry.column_names.include?(params[:sort]) ? params[:sort] : "entry_date"
    end
    
    def sort_direction
      %w[asc desc].include?(params[:direction]) ? params[:direction] : "desc"
    end
end
