module ApplicationHelper
  # Returns the full title on a per-page basis.
  def full_title(page_title = '')
    base_title = I18n.t('application_title')
    if page_title.empty?
      base_title
    else
      page_title + " | " + base_title
    end
  end

  # Returns a hash for an array for use with select
  def get_hash_for_select(arr)
    arr.to_enum(:each_with_index).map{|a,i| [a,i]}
  end

  # Creates a row for a table with two columns containing title and content
  def table_row_helper(title, content)
    tmp = ""
    tmp = "<tr><td><b>".html_safe + title + "</b></td><td>".html_safe + content + "</td><tr>".html_safe unless content.blank?
    return tmp
  end
  
  # Creates a sortable column header
  def sortable(column, title = nil)
    title ||= column.titleize
    css_class = column == sort_column ? "current #{sort_direction}" : nil
    direction = column == sort_column && sort_direction == "asc" ? "desc" : "asc"
    link_to title, {:sort => column, :direction => direction}, {:class => css_class}
  end
end
