class JournalEntry < ActiveRecord::Base
  belongs_to :user
  
  def decodeArrowAt(index)
    if index < self.half_round.length
      arr = self.half_round[index]
      if(arr == "T")
        return "10"
      elsif arr === "M" || arr === "X" || arr === "1" || arr === "2" || arr === "3" || arr === "4" || 
                 arr === "5" || arr === "6" || arr === "7" || arr === "8" || arr === "9"
        return arr
      end
    end
    return "_"
  end
  
  def arrowValueAt(index)
    if index < self.half_round.length
      arr = self.half_round[index]
      if(arr == "T" || arr == "X")
        return 10
      elsif arr === "1" || arr === "2" || arr === "3" || arr === "4" || arr === "5" || arr === "6" || arr === "7" || arr === "8" || arr === "9"
        return arr.to_i
      end
    end
    return 0
  end
end